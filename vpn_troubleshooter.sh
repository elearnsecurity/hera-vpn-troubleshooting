#!/usr/bin/env bash
# eLearnSecurity Connectivity Troubleshooting Script
# 0.8b
# Usage: chmod +x vpn_troubleshooter.sh && ./vpn_troubleshooter.sh

#from https://github.com/kvz/bash3boilerplate
#require at leat bash 3.x 
if [[ "${BASH_VERSINFO[0]}"  -lt  "3" ]]  ;   then echo "bash version < 3" ; exit 1 ; fi


# debug mode
if [[ "${@}" = *"-d"* ]]; then
  set -o xtrace
fi


RE='\033[0;31m'
OR='\033[0;33m'
N='\033[0m'

# eLS banner

banner=$(base64 -d <<<"ICAgICAgXyAgICAgICAgICAgICAgICAgICAgICAgICAgIF9fX19fICAgICAgICAgICAgICAgICAgICAgIF8gXyAgICAgICAgIAogICAgIHwgfCAgICAgICAgICAgICAgICAgICAgICAgICAvICBfX198ICAgICAgICAgICAgICAgICAgICAoXykgfCAgICAgICAgCiAgX19ffCB8ICAgICBfX18gIF9fIF8gXyBfXyBfIF9fIFwgYC0tLiAgX19fICBfX18gXyAgIF8gXyBfXyBffCB8XyBfICAgXyAKIC8gXyBcIHwgICAgLyBfIFwvIF9gIHwgJ19ffCAnXyBcIGAtLS4gXC8gXyBcLyBfX3wgfCB8IHwgJ19ffCB8IF9ffCB8IHwgfAp8ICBfXy8gfF9fX3wgIF9fLyAoX3wgfCB8ICB8IHwgfCAvXF9fLyAvICBfXy8gKF9ffCB8X3wgfCB8ICB8IHwgfF98IHxffCB8CiBcX19fXF9fX19fL1xfX198XF9fLF98X3wgIHxffCB8X1xfX19fLyBcX19ffFxfX198XF9fLF98X3wgIHxffFxfX3xcX18sIHwKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfXy8gfAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfF9fXy8gCgo=")

echo -e "$OR$banner$N"
echo -e "==================================================="
echo -e "  eLearnSecurity VPN Connectivity Troubleshooter   "
echo -e "==================================================="
echo -e "NOTE: BEFORE RUNNING THIS SCRIPT, MAKE SURE THAT:

1. You're connected to the internet.
2. Your current lab in the members portal is in a 'Started' state. 
3. You have downloaded the lab ovpn file and it is in the same directory as this script.
4. You have your VPN credentials on-hand. This script will attempt a connection to your lab and will prompt you for your VPN credentials.\n

$RE DISCLAIMER$N: By using this script, you agree to the collection of the following general information from your current working machine:

- Current OVPN Lab File Information
- Current Effective User ID (EUID)
- Your external IP address (ISP)
- Operating System Information/Version
- Your current date/timezone
- Network Interface(s) Information
- Current Network Routes
- Current DNS Server
- Current Internet Service Provider name\n"

echo -e "NOTE : this script will also kill all your existing OpenVPN connections , so please save your work if you are working on remote data"


read -n 1 -s -r -p "Press any key to agree to the above and continue or ctrl-c to abort..."

# check for ovpn file(s)
function _updateTS () {
export date=$(/bin/date "+%Y%m%d" )
export time=$(/bin/date "+%H%M%S" )
export now="${date}_${time}"
}
_updateTS

ovpn_file=$(find .. -type f -name '*.ovpn' | wc -l)


if [[ "$ovpn_file" == "1" ]]; then
	echo -e "\n\n[+] OVPN FILE CHECK PASSED"

elif [[ "$ovpn_file" == "0" ]] || [[ "$ovpn_file" -gt "1" ]]; then
	echo -e "$RE\n\nCould not find OVPN file in current directory, or there are multiple OVPN files. Please remove duplicates and make sure you run this script from the same location as your OVPN file for started lab.$N"
	exit 1
fi

log=hera_vpn_troubleshooting_${now}
student_id=$(ls -l *.ovpn |cut -d"-" -f2 |cut -f1 -d".")

# get current user
echo -e "[+] Checking permissions"
echo -e "\n\n======================= Is root?  =======================" > $log

if [[ $EUID -ne "0" ]]; then
        echo -e "$RE[WARN] This script must be run as root, or use sudo.$N"
        exit 1
fi

id >> $log

# kill any existing VPN connection 
killall --signal SIGINT openvpn

# internet check
echo -e "[+] Checking internet connection"

if ($( ping -c 1 8.8.8.8 > /dev/null )); then
	echo -e "[+] Connection Check Passed"
else
	echo -e "$RE[!] Internet Check Failed. Please connect to the internet before running this script.$N"
	exit 1
fi
sleep 2

# Get Operating System Information
echo -e "[+] Checking Operating System"
echo -e "\n\n======================= Operating System =======================" >> $log

cat /etc/issue >> $log
cat /etc/*-release >>  $log
sleep 2

# Get date
echo -e "[+] Getting Current Date"
echo -e "\n\n======================= Current Date =======================" >> $log

date >> $log
sleep 2

# Get Interfaces Information
echo -e "[+] Checking Network Interfaces"
echo -e "\n\n======================= Network Interfaces =======================" >> $log

ifconfig -a >>  $log
sleep 2

# Get current routing information
echo -e "[+] Getting Network Routes"
echo -e "\n\n======================= Network Routes =======================" >> $log

route -n >> $log
sleep 2
# Get DNS Information
echo -e "[+] Getting DNS Information"
echo -e "\n\n======================= DNS Information =======================" >> $log

cat /etc/resolv.conf >> $log
sleep 2

echo -e "[+] Getting connection information"
echo -e "\n\n======================= Connection Information (ss) PRE-VPN  connection =======================" >> $log
ss  -lanp >> $log
echo -e "\n\n======================= Connection Information (lsof) PRE-VPN  connection =======================" >> $log
lsof  -i  -n -P >> $log
sleep 2

echo -e "\n\n======================= Service status PRE-VPN  connection =======================" >> $log
systemctl status >> $log
sleep 2

echo -e "\n\n======================= Process status PRE-VPN  connection =======================" >> $log
pstree -p >> $log
sleep 2
ps -aux >> $log



# Ping
echo -e "[+] Checking ping Connectivity"
gateway=$(ip -4 route list 0/0 |cut -f3 -d" ")
vpn_srv_ip=$(grep -e '^remote ' *.ovpn | awk '{print $2}')

# ping the gateway
echo -e "\n\n======================= Gateway Connectivity Results =======================" >> $log
ping -c 4 "$gateway" >> $log

# ping the eLS VPN server from the OVPN file
echo -e "\n\n ======================= Ping eLS Server Results =======================" >> $log
ping -c 4 $vpn_srv_ip >> $log
sleep 2

# external IP and WHOIS lookup for determining ISP's that block VPN traffic
ext_ip=$(curl -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36" -s http://ipecho.net/plain)

echo -e "[+] Getting external IP"
echo -e "\n\n======================= External IP =======================" >> $log

curl -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36" -s http://ipecho.net/plain >> $log
sleep 2

echo -e "[+] Getting Current ISP"
echo -e "\n\n======================= ISP =======================" >> $log
for ip in $ext_ip; do whois $ext_ip; done |grep OrgTechName >> $log
sleep 2

# UDP port connectivity
echo -e "[+] Checking UDP Port Connectivity to HERA Lab Network"
echo -e "\n\n======================= UDP Port Connectivity Results =======================" >> $log

ip=$(host $(grep -e '^remote ' *.ovpn | awk '{print $2}')| awk '{print $4}' )
vpn_port=$(grep -e '^remote ' *.ovpn | awk '{print $3}')

nc -vzun $ip $vpn_port 2>&1 | tee -a $log
sleep 2

# attempt OPENVPN connection and log output
echo -e "[+] Generating VPN Connectivity Log"
echo -e "\n\n======================= VPN Connectivity Log =======================" >> $log

ovpn_file=$(find . -name '*.ovpn' )
	
echo -e "============= Please Enter your HERA Lab VPN Credentials ============="
	
openvpn --config "$ovpn_file" --daemon --log /tmp/vpn_connection_log 2>&1
echo -e "\nPlease wait while we attempt connection to HERA labs...."
sleep 10
connection_log=$(cat /tmp/vpn_connection_log)
printf '%s' "$connection_log" >> $log
rm /tmp/vpn_connection_log
sleep 2

# check that TAP exists after VPN connection.

echo -e "[+] Checking for existence of TAP interface"
echo -e "\n\n======================= Network Interfaces POST-VPN connection =======================" >> $log

ifconfig -a >> $log
sleep 2

echo -e "[+] Collecting iptables rules"
echo -e "\n\n======================= Getting iptables rules  =======================" >> $log
iptables -L -n -v 

echo -e "[+] Getting connection information"
echo -e "\n\n======================= Connection Information (ss) POST-VPN  connection =======================" >> $log
ss  -lanp >> $log
#netstat -lunpta >> $log
sleep 2
echo -e "\n\n======================= Connection Information (lsof) POST-VPN  connection =======================" >> $log
lsof  -i  -n -P >> $log

echo -e "\n\n======================= Service status POST-VPN  connection =======================" >> $log
systemctl status >> $log
sleep 2

echo -e "\n\n======================= Process status POST-VPN  connection =======================" >> $log
pstree -p >> $log
sleep 2
ps -aux >> $log


# kill the vpn connection so the script can continue
vpn_pid=$(ps a | grep [o]penvpn |awk '{print $1}')

for pid in $vpn_pid; do
	kill -SIGINT $pid
done

mv $log $log.txt

echo -e "[+] Finished."
echo -e "[+] Finished." >> $log.txt
chmod 644 $log.txt
echo -e "$RE[+] Please send the log file '$log.txt' to 'support@elearnsecurity.com'.$N"
